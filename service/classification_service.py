from models import Response
import codecs
import os.path

FILE_PATH = "/home/ptphu/Documents/meeting_assistance/program/text/"

count = 1


class Classification_Service():
    def __init__(self):
        self.name = ""
        self.content = ""
        self.id_ = count

    def identify(self):
        if (os.path.isfile(FILE_PATH + str(count) + ".txt")):
            f = codecs.open(FILE_PATH + str(count) + ".txt", 'r', 'utf-8-sig')
            global count
            text = f.readlines()
            text = text[0]
            count = count + 1
            tmp_content = text.split("_")[1]
            self.name = text.split("_")[0]
            self.content = tmp_content[0:len(tmp_content) - 1]
            self.id_ = count
            print self.name
            print self.content
            print self.id_
            return Response(self.id_, self.__classify(), self.__recognize())

    def __recognize(self):
        return self.content

    def __classify(self):
        return self.name

a = Classification_Service()
a.identify()