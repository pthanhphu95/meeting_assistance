from datetime import datetime


class Response():
    def __init__(self, id_=None, name=None, content=None, created=None):
        self.id = id_
        self.name = name
        self.content = content
        self.created = created or datetime.now()
