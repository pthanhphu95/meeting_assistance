from rest_framework import serializers

class MainSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    name = serializers.CharField()
    content = serializers.CharField()
    created = serializers.DateTimeField()