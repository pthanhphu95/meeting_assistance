from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response

from service.classification_service import Classification_Service
from service.main_service import Main_Service
from api.serializers import MainSerializer
from threading import Thread
import threading


@api_view(['GET', 'POST'])
def classifier(request):
    if request.method == 'GET':
        service = Classification_Service()
        reponse = service.identify()
        if reponse == None:
            return Response('Cannot detect speaker', status=status.HTTP_200_OK)

        serializer = MainSerializer(reponse)
        return Response(serializer.data)

    elif request.method == 'POST':
        return Response('Failed', status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET'])
def start_server(request):
    print threading.enumerate()
    main_service = Main_Service()
    main_service.start()
    return Response('Program is started', status=status.HTTP_200_OK)


