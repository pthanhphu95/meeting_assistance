import tensorflow as tf
import numpy as np
import load_file as loadfile
import yaml

yamlfile = open("/home/ptphu/Documents/meeting_assistance/program/config.yaml", 'r')
cfg = yaml.load(yamlfile)

n_dim = cfg["n_dim"]
n_hidden_units_one = cfg["n_hidden_units_one"]
n_hidden_units_two = cfg["n_hidden_units_two"]
sd = 1 / np.sqrt(n_dim)
learning_rate = cfg["learning_rate"]
batch_size = cfg["batch_size"]
n_classes = cfg["n_classes"]
train_dataset_folder = cfg["train_dataset_folder"]
validate_dataset_folder = cfg["validate_dataset_folder"]
model_folder = cfg["model_folder"]


def train_model_w_new_data():
    X = tf.placeholder(tf.float32, [None, n_dim])
    Y = tf.placeholder(tf.float32, [None, n_classes])

    W_1 = tf.Variable(tf.random_normal([n_dim, n_hidden_units_one], mean=0, stddev=sd))
    b_1 = tf.Variable(tf.random_normal([n_hidden_units_one], mean=0, stddev=sd))
    h_1 = tf.nn.tanh(tf.matmul(X, W_1) + b_1)

    W_2 = tf.Variable(tf.random_normal([n_hidden_units_one, n_hidden_units_two], mean=0, stddev=sd))
    b_2 = tf.Variable(tf.random_normal([n_hidden_units_two], mean=0, stddev=sd))
    h_2 = tf.nn.sigmoid(tf.matmul(h_1, W_2) + b_2)

    W = tf.Variable(tf.random_normal([n_hidden_units_two, n_classes], mean=0, stddev=sd))
    b = tf.Variable(tf.random_normal([n_classes], mean=0, stddev=sd))
    y_ = tf.nn.softmax(tf.matmul(h_2, W) + b)
    sess = tf.InteractiveSession()
    sess.run(tf.global_variables_initializer())

    #
    correct_prediction = tf.equal(tf.argmax(y_, 1), tf.argmax(Y, 1))
    max_prediction = tf.argmax(y_, 1)
    accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))
    cost_function = tf.reduce_mean(-tf.reduce_sum(Y * tf.log(y_)))
    optimizer = tf.train.GradientDescentOptimizer(learning_rate).minimize(cost_function)
    features, labels = loadfile.get_audio_features("./", train_dataset_folder)
    labels = loadfile.one_hot_encode_with_test_file(labels, n_classes)
    features_validate, labels_validate = loadfile.get_audio_features("./", validate_dataset_folder)
    labels_validate = loadfile.one_hot_encode_with_test_file(labels_validate, n_classes)

    print labels
    nSteps = 3000
    for i in range(nSteps):
        # run the training step with feed of data
        offset = (nSteps * batch_size) % (features.shape[0] - batch_size)
        # Generate a minibatch.
        batch_data = features[offset:(offset + batch_size), :]
        batch_labels = labels[offset:(offset + batch_size), :]
        optimizer.run(feed_dict={X: features, Y: labels})
        if (i + 1) % 10 == 0:
            y_pred = sess.run(accuracy, feed_dict={X: features_validate, Y: labels_validate})
            print y_pred
    save_path = tf.train.Saver().save(sess, model_folder)
    print("Model saved")

print '************** Train *************'
train_model_w_new_data()
